import streamlit as st
import pages
import imp

class App:
    
    def __init__(self, name, title):
        self.name = name
        self.title = title
        self.state = {}

        self.init_pages()
        
    def init_pages(self):
        self.pages = {}
        for page in pages.__all__:
            p = page(self.state)
            self.pages[p.name] = p
        return
            
    def main(self):        
        st.sidebar.title(self.title)
        
        name = st.sidebar.radio("Select page: ", tuple(self.pages.keys()))        
        self.pages[name].main()
