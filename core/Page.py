import streamlit as st

class Page:
    
    def __init__(self, name, title, state):
        self.name = name
        self.title = title
        self.state = state.get(name, {})
        
    def main(self):        
        st.write(self.title)
        return 
