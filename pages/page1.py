import streamlit as st

from core.Page import Page

class Page1(Page):
    def __init__(self, state):
        super().__init__("Page1", "First Page", state)

    def main(self):
        super().main()

        st.write("page 1 content")
