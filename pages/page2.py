import streamlit as st
from core.Page import Page

class Page2(Page):
    def __init__(self, state):
        super().__init__("Page2", "Second Page", state)

    def main(self):
        super().main()

        st.write("page 2 content")
